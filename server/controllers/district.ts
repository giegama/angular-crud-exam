import BaseCtrl from './base';
import District from '../models/district';

export default class DistrictCtrl extends BaseCtrl {
  model = District;
}
