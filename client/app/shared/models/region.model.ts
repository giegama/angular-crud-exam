export class Region {
   result?: any;
   items?: any;
}

export class Province {
    id?: string;
    nama?: string;
}

export class District {
    id?: string;
    nama?: string;
}
